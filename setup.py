from setuptools import find_packages, setup

setup(
    name="dagster_paramiko",
    version="0.2",
    packages=['dagster_paramiko'],
    install_requires=[
        'paramiko',
    ],
)
